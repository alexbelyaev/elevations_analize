# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 17:49:45 2021

@author: belyaev
"""

import numpy as np
import matplotlib.pyplot as plt
import glob


for k in range(21):

    base_route = 'D:/work/datasets'
    routeTxtFile = base_route + '/big_corner_to_64.txt'
    # uncomment this line to upload 64-pulses data
    # (and go to 59 line)
    route_pc2_files = 'D:/work/datasets/radar_pc2_64/pc2_' + str(k)
    # uncomment this line to upload 32-pulses data
    # (and go to 59 line)
#    route_pc2_files = 'D:/work/datasets/radar_pc2_32/pc2_' + str(k)
    radar_pc2_frames = glob.glob(route_pc2_files + '/*.npy')
    radar_pc2 = []

    f = open(routeTxtFile, 'r')
    list_ = f.readlines()
    f.close()

    num_pc2_frames = np.shape(radar_pc2_frames)[0]
    for i in range(num_pc2_frames):
        radar_pc2.append(np.load(radar_pc2_frames[i]))
        #    plt.plot(radar_pc2[i][])
        x_mas = []
        y_mas = []
        z_mas = []
        ampl_mas = []
        mas = []
    # i = 0
        pc2_shape = np.shape(radar_pc2[i])[0]
        x_mas = radar_pc2[i][:, 0]
        y_mas = radar_pc2[i][:, 1]
        z_mas = radar_pc2[i][:, 2]
        ampl_mas = radar_pc2[i][:, 4]

        corner_x_mas = []
        corner_y_mas = []
        corner_z_mas = []
        corner_ampl_mas = []

        for j in range(pc2_shape):
            if ((y_mas[j] < 0.9) and (y_mas[j] > -0.9)):
                corner_x_mas.append(x_mas[j])
                corner_y_mas.append(y_mas[j])
                corner_z_mas.append(z_mas[j])
                corner_ampl_mas.append(ampl_mas[j])

        # plt.figure()
        # uncomment this line to upload 64-pulses data
        plt.plot(corner_x_mas, corner_z_mas, 'bx')
        # uncomment this line to upload 62-pulses data
        # plt.plot(corner_x_mas, corner_z_mas, 'ro')
        plt.grid("on")
        plt.xlabel("x")
        plt.ylabel("z")
        plt.title("red - 32; blue - 64")

        new_pc2_shape = np.shape(corner_x_mas)[0]

#        for j in range(new_pc2_shape): 
#            ann = "%.4f" % (corner_ampl_mas[j]/1e16)
#
#            plt.annotate(ann, (corner_x_mas[j]+0.01,
#                               corner_z_mas[j]+0.01), size=8)
